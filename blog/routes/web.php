<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'MasterController@Master');
Route::get('/utama', 'AuthController@Utama');
Route::get('/table', 'AuthController@Table');
Route::get('/data-table', 'AuthController@DataTable');

//CRUD

//Create Data Cast
Route::get('/cast/create', 'CastController@Create');
//untuk kirim inputan
Route::post('/cast','CastController@store');

//Read Data Cast
//Tampil Semua Data Cast
Route::get('/cast','CastController@index');
//Detail Cast Berdasarkan ID
Route::get('/cast/{id}','CastController@show');

//Update Data Cast

//masuk ke form Cast berdasarkan ID
Route::get('/cast/{id}/edit','CastController@edit');
//untuk update data inputan berdasarkan ID
Route::put('/cast/{id}','CastController@update');

//Delete Data Cast

//Delete Data Berdasarkan ID
Route::delete('/cast/{id}','CastController@destroy');