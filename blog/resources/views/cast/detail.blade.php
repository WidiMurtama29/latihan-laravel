@extends('master')
@section('judul')
    Halaman Detail
@endsection
@section('judul2')
    Halaman Detail Cast
@endsection
@section('content')

<h2>Nama Lengkap</h2>
<h1 class="text-primary">{{$cast->nama}}</h1>
<h3>Umur</h3>
<p>{{$cast->umur}}</p>
<h3>Bio</h3> 
<p>{{$cast->bio}}</p> 
<a href="/cast" class="btn btn-info btn-sm">Kembali</a>

@endsection

