<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function Create()
    {
        return view('cast.create');
    }
    public function store(Request $request)
    {
        //test($request->all());
        $request->validate([
            'nama' => 'required|',
            'umur' => 'required|max:2',
            'bio' => 'required|max:255',
        ],
        [
            'nama.required' => 'Nama Cast Harus Diisi Tidak Boleh Kosong!',
            'umur.required' => 'Umur Cast Harus Diisi! Tidak Boleh Kosong!',
            'bio.required' => 'Bio Cast Harus Diisi! Tidak Boleh Kosong!',
        ]
        );

        DB::table('cast')->insert(
            ['nama' => $request['nama'], 
             'umur' => $request['umur'],
             'bio' => $request['bio'],
            ]
        );

        return redirect('/cast');
            
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
 
        return view('cast.index', compact('cast'));
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.detail', compact('cast')); 
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();

        return view('cast.update', compact('cast')); 
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|',
            'umur' => 'required|max:2',
            'bio' => 'required|max:255',
        ],
        [
            'nama.required' => 'Nama Cast Harus Diisi Tidak Boleh Kosong!',
            'umur.required' => 'Umur Cast Harus Diisi! Tidak Boleh Kosong!',
            'bio.required' => 'Bio Cast Harus Diisi! Tidak Boleh Kosong!',
        ]
        );

        DB::table('cast')
              ->where('id', $id)
              ->update(
                ['nama' => $request['nama'],
                 'umur' => $request['umur'],
                 'bio' => $request['bio']
                ]
            );
        return redirect('/cast');    
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();

        return redirect('/cast');
    }
}
